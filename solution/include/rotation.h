#ifndef ROTATION_H
#define ROTATION_H

#include "image.h"
struct image rotate(struct image* source, int angle);
#endif
