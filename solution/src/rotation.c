#include "rotation.h"
#include "image.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

struct image rotate(struct image *source, int angle) {
    angle = (angle + 360) % 360;
    struct image rotated = *source;
    uint64_t newWidth, newHeight;
    if (angle == 90 || angle == 270) {
        newWidth = rotated.height;
        newHeight = rotated.width;
    } else {
        newWidth = rotated.width;
        newHeight = rotated.height;
    }
    
        
    if(angle != 0 ) {
         struct pixel *tempData ;
        tempData = (struct pixel *)malloc(newWidth * newHeight * sizeof(struct pixel));
         if (!tempData) {
             perror("Error allocating memory");
         exit(EXIT_FAILURE);
         }
        for (int y = 0; y < rotated.height; ++y) {
            for (int x = 0; x < rotated.width; ++x) {
                uint64_t sourceIndex = y * rotated.width + x;
                uint64_t destIndex;

                switch (angle) {
                    case 90:
                        destIndex = (rotated.width - x - 1) * newWidth + y;
                        break;
                    case -90:
                        destIndex = x * newWidth + rotated.height - y - 1;
                        break;
                    case 180:
                        destIndex = (rotated.height - y - 1) * rotated.width + rotated.width - x - 1;
                        break;
                    case -180:
                        destIndex = y * rotated.width + x;
                        break;
                    case 270:
                        destIndex = x * newWidth + rotated.height - y - 1;
                        break;
                    case -270:
                        destIndex = (rotated.width - x - 1) * newWidth + y;
                        break;
                    default:
                        perror("Unsupported rotation angle");
                        exit(EXIT_FAILURE);
                }
                tempData[destIndex] = rotated.data[sourceIndex];
            }
        }
        
    rotated.width = newWidth;
    rotated.height = newHeight;

    free(rotated.data);

    rotated.data = tempData;

    return rotated;
    }
    else{
        
            return *source;
        
    }

    
}

