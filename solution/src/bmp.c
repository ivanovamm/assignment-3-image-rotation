#include "bmp.h"
#include <inttypes.h>
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;
    int result = (int)fread(&header, sizeof(struct bmp_header), 1, in);
    if (result!=1) {
        return READ_INVALID_HEADER;
    }
    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data = malloc(img->width * img->height * sizeof(struct pixel));

    if (!img->data) {
        return READ_OUT_OF_MEMORY;
    }

    for (uint32_t row = 0; row < header.biHeight; ++row) {
        
        int img_result = (int)fread(img->data + row * img->width, sizeof(struct pixel), header.biWidth, in);
        if (img_result != img->width) {
            free(img->data);
            return 1;
        }
        
        if(fseek(in, (long)(4 - (img->width * sizeof(struct pixel)) % 4) % 4, SEEK_CUR)!=0){
            free(img->data);
            return 1;
        }
    }
    
    return READ_OK;
}



enum write_status to_bmp(FILE* out, struct image* img) {
    struct bmp_header header;
    header.bfType = 0x4D42;
    header.bfileSize = sizeof(struct bmp_header) + img->width * img->height * sizeof(struct pixel);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    uint8_t padding = (4 - (header.biWidth * sizeof(struct pixel)) % 4) % 4;
    header.biSizeImage = (img->width * sizeof(struct pixel) + padding) * img->height;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    int header_result = (int)fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (header_result!= 1) {
        return 1;
    }
    
    uint32_t i = 0;
    while (i < img->height) {
        int img_result = (int)fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out); 
        if (img_result != img->width) {
            return 1;
        }
        if (padding > 0) {
                uint8_t paddingData[3] = {0};
                int padding_result = (int)fwrite(paddingData, 1, padding, out);
                if  (padding_result != padding) {
                return 1;
            }
        }
        i+=1;
    }
    
    return WRITE_OK;
}

