#include "bmp.h"
#include "rotation.h" 
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
 

int main(int argc, char* argv[]) {
    if (argc!=4){
        printf("invalid format");
    }
    FILE* input_file = fopen(argv[1] , "rb");
    if (!input_file) {
        perror("Error opening file");
        return 1;
    }
    struct image original_image;
    enum read_status read_result = from_bmp(input_file, &original_image);
    if (read_result != READ_OK) {
        fclose(input_file);
        return 1;
    }
    int angle = atoi(argv[3]);
    struct image rotated_image;
    rotated_image = rotate(&original_image, angle);
    fclose(input_file);
    FILE* output_file = fopen(argv[2], "wb");
    if (!output_file) {
        perror("Error opening output file");
        destroy_image(&rotated_image);
        destroy_image(&original_image);
        return 1;
    }

    enum write_status write_result = to_bmp(output_file, &rotated_image);
    if (write_result != WRITE_OK) {
        fprintf(stderr, "Error writing BMP file: %d\n", write_result);
        fclose(output_file);
        return 1;
    }
    fclose(output_file);
    destroy_image(&rotated_image);
    printf("Image rotated and saved successfully\n");
}
